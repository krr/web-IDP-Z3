
*************************************

         D E P R E C A T E D

*************************************

This repository is now deprecated.

Its content  has been moved to https://gitlab.com/krr (in the `idp_web_client` folder)